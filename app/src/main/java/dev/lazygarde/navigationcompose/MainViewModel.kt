package dev.lazygarde.navigationcompose

import androidx.lifecycle.ViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow


class MainViewModel : ViewModel() {
    private val _screenRoot = MutableStateFlow(ScreenRoot.HOME)
    val screenRoot = _screenRoot.asStateFlow()

    fun updateRoot(root: ScreenRoot) {
        _screenRoot.value = root
    }
}

enum class ScreenRoot {
    HOME, SETTINGS
}