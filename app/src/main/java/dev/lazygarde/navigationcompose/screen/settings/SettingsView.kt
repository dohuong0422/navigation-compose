package dev.lazygarde.navigationcompose.screen.settings

import androidx.activity.compose.BackHandler
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import dev.lazygarde.navigationcompose.MainViewModel
import dev.lazygarde.navigationcompose.ScreenRoot


@Composable
fun SettingsView(viewModel: MainViewModel) {
    BackHandler {
        viewModel.updateRoot(ScreenRoot.HOME)
    }
    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        Text(text = "Settings")
    }
}
