package dev.lazygarde.navigationcompose.screen.home

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import dev.lazygarde.navigationcompose.MainViewModel
import dev.lazygarde.navigationcompose.ScreenRoot


@Composable
fun HomeView(viewModel: MainViewModel) {
    Box(modifier = Modifier.fillMaxSize().padding(20.dp)) {
        Text(text = "Home", modifier = Modifier.align(Alignment.Center))
        Image(
            imageVector = Icons.Default.Settings,
            contentDescription = null,
            modifier = Modifier
                .align(Alignment.TopEnd)
                .clickable {
                    viewModel.updateRoot(ScreenRoot.SETTINGS)
                }
        )
    }
}