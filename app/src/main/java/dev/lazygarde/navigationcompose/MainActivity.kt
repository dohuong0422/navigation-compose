package dev.lazygarde.navigationcompose


import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.enableEdgeToEdge
import androidx.activity.viewModels
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import dev.lazygarde.navigationcompose.screen.home.HomeView
import dev.lazygarde.navigationcompose.screen.settings.SettingsView

class MainActivity : ComponentActivity() {

    private val viewModel: MainViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        enableEdgeToEdge()
        setContent {
            val screenRoot by viewModel.screenRoot.collectAsState()
            when (screenRoot) {
                ScreenRoot.HOME -> HomeView(viewModel)
                ScreenRoot.SETTINGS -> SettingsView(viewModel)
            }
        }
    }
}

